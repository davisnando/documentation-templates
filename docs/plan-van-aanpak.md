# Plan van Aanpak (__Projectnaam__)

```text
Leereenheid: NAAM LEEREENHEID
Versienummer: 1.0
Auteur(s): AUTEUR
Datom: DATUM
```

```text
COMPETENTIES

1. Competentie ‘Plannen en organiseren’
    a. Stelt een plan van aanpak en een chronologische planning van de uit te
       voeren activiteiten op voor de beschikbare tijd
    b. Stelt mensen en middelen vast die nodig zijn om de activiteiten uit te
       voeren
2. Competentie ‘Formuleren en rapporteren’
    c. Documenteert het plan van aanpak en laat deze accorderen door de
       leidinggevende/projectleider
3. Competentie ‘Samenwerken en overleggen’
    d. Stemt in twee tot drie gesprekken het plan van aanpak af met de
       leidinggevende/projectleider
```

## Versiebeheer

| Datum | Versie | Wijziging | Wie |
| - | - | - | - |
| DATUM | 1.0 | Initial version | Bram |

## Inleiding

Dit document geeft in een kort overzicht weer hoe ik, als opdrachtnemer, deze
opdracht ga aanpakken. Ik zal duidelijk maken met welke middelen ik ga werken en
wat de doorlooptijd is voor dit project.

> *Beschrijf in het kort de doelstelling van het project*

## Achtergrond

> *(WIE) Maak een korte beschrijving van de organisatie/opdrachtgever waarin het
project zich afspeelt? Beschrijf alle betrokken partijen en maak kenbaar wat de
relatie is en rolverdeling?*

## Doelstelling

> *(WAT) Wat wil de opdrachtgever bereiken met het product. Wat zijn de voordelen
voor de opdrachtgever.*

## Opdracht

> *(HOE) Omschrijving van wat er wordt verwacht.*
>
> *Wat wordt het resultaat of eindproduct.*
>
> *Geeft aan wat het einddoel van een product is.*
>
> *Benoem een opdrachtgever en opdrachtnemer.*

## Projectactiviteiten

> *Welke activiteiten gaan worden ondernomen en in welke volgorde.*

## Projectgrenzen en randvoorwaarden

> *Afbakenen wat er wel en niet gedaan wordt binnen dit project.*

## Producten

> *Opsomming van alle producten die opgeleverd gaan worden voor dit project.
Vooral documentatie (Functioneel ontwerp, technisch ontwerp, implementatieplan,
acceptatietestplan, beheer en onderhourdsplan).*

## Kwaliteit

> *Hoe ga je zorg dragen dat alle opgeleverde producten voldoen aan de eisen en
wensen van de opdrachtgever.*

## Projectorganisatie

> *Vaststellen en verdelen van functies. Hou rekening met bevoegdheid,
verantwoordelijkheid en communicatie. Wie is waarvoor verantwoordelijk binnen
het project?*

## Planning

> *Overzicht van de uit te voeren activiteiten over het gehele project. Wie
voert wat wanneer uit. Planning bij voorkeur in strokenplanning, zie
bijgeleverde strokenplanning.*

## Kosten en Baten

> *Afweging van kosten en baten, is het wel haalbaar om het project uit te voeren.
Wat levert het op?*

## Akkoord leidinggevende / Projectleider

Naam

---

Datum

---

Handtekening

---