# Informatiebehoefte (__Projectnaam__)

```text
Leereenheid: NAAM LEEREENHEID
Versienummer: 1.0
Auteur(s): AUTEUR
Datom: DATUM
```

```text
COMPETENTIES

1. Competentie `Onderzoeken`
    a. Stelt een geprioriteerde wensen en eisenlijst samen van de behoeften en/of
       wensen van de opdrachtgever door één tot twee gesprekken
2. Competentie ‘Analyseren’
    b. Analyseert de verzamelde behoeften en/of wensen van de opdrachtgever om
       informatie uit af te leiden voor het opstellen van een ontwerp
    c. Toetst de verkregen informatie op (on)mogelijkheden
3. Competentie ‘Creëren en innoveren’
    d. Herkent en introduceert veranderingen
    e. Verwerkt veranderingen (later) in een functioneel en technisch ontwerp
4. Competentie ‘Op de behoeften en verwachtingen van de "klant" richten’
    f. Blijft door de gesprekken bij 1a, gericht op de behoeften en verwachtingen
       van de opdrachtgever
```

## Versiebeheer

| Datum | Versie | Wijziging | Wie |
| - | - | - | - |
| DATUM | 1.0 | Initial version | Bram |

## Inleiding

Dit document heeft tot doel om de initiële informatiebehoefte van de
opdrachtgever te verwoorden. Daarnaast zal dit document op een heldere manier,
via de MoSCoW methodiek, de wensen eisen van de opdrachtgever beschrijven en
deze prioriteren.

> *Beschrijf in het kort de doelstelling van het project.*

## Behoeftebeschrijving opdrachtgever

> *Maak een korte beschrijving van de behoefte van de opdrachtgever.*

## MoSCoW prioritering en subprioritering van wensen en eisen

De __MoSCoW__ methodiek is een simpele methodiek die vaak wordt gebruikt in de
zakenwereld om aan te geven hoe belangrijk het is dat een bepaalde eis voltooid
wordt.

__M__ = Must have, deze eis moet voltooid zijn voordat de applicatie een succes
genoemd kan worden.

__S__ = Should have, een eis die vaak ook van hoge prioriteit is, soms kan deze
weggelaten worden.

__C__ = Could have, een eis die wel gewild is, maar niet perse nodig is.

__W__ = Won’t have, een eis die niet zomaar in een release zal verschijnen, maar
die in de toekomst misschien nog wel toegevoegd wordt.

Binnen de verschillende prioriteitstellingen kan nog een subprioritering worden
aangegeven om de volgorde van belangrijkheid te bepalen. Dit gebeurt door een
cijfer te plaatsen achter de prioritering.

| Functie/Wens | MoSCoW prioritering | Sub-Prioritering
| - | - | - |
| Inloggen | M | 1 |
| Rollen beheren | M | 2 |
|  | S | 3 |
|  | C | 4 |
|  | W | 5 |

## Onmogelijkheden van het project

*Beschrijf de onmogelijkheden binnen dit project. Wat kan er niet worden opgeleverd
aan de opdrachtgever.*

## Werkwijze gespreksverslag vastlegging

> *Omschrijf hoe verslagen worden genotuleerd en opgeslagen. Tevens de manier van
aanleveren van verslagen beschrijven.*
>
> *Communicatie tussen opdrachtgever/opdrachtnemer beschrijven.*