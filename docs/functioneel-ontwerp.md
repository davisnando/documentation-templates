# Functioneel Ontwerp (__Projectnaam__)

```text
Leereenheid: NAAM LEEREENHEID
Versienummer: 1.0
Auteur(s): AUTEUR
Datom: DATUM
```

```text
COMPETENTIES

1. Competentie ‘Plannen en organiseren’
    a. Stelt doelen en een chronologische planning van de benodigde tijd op van de
       activiteiten voor het opleveren van het ontwerp
    b. Stelt mensen en middelen vast die nodig zijn voor het opleveren van het ontwerp
2. Competentie ‘Analyseren’
    c. Zet de verkregen informatie (uit opdracht 1) om in oplossingen voor de
       behoeften/wensen van de opdrachtgever. Verwerkt deze in het ontwerp
3. Competentie ‘Formuleren en rapporteren’
    d) Documenteert het functionele ontwerp
4. Competentie ‘Overtuigen en beïnvloeden’
    e) Komt met ideeën gericht op de behoeften/wensen
    f) Brengt deze ideeën onderbouwend en beargumenterend over
5. Competentie ‘Presenteren’
    g) Presenteert het functionele ontwerp aan de opdrachtgever
```

## Versiebeheer

| Datum | Versie | Wijziging | Wie |
| - | - | - | - |
| DATUM | 1.0 | Initial version | Bram |


## Inleiding

Het doel van dit document is om een overzicht te geven van de functies die de
applicatie moet kunnen uitvoeren.

De op te leveren software zal in zijn geheel worden opgeleverd waarna de
algehele gebruikerstest zal plaatsvinden.

Alle functies binnen de software zijn in dit document beschreven, de software
zal dus ook geen andere functies bevatten dat deze die hieronder in use cases
zijn beschreven.

> *Beschijf het project en de te maken stappen binnen deze fase.*
>
> *Beschrijf hoe de applicatie samenwerkt met bestaande software binnen de organisatie.*
>
> *Beschrijf waar binnen de organisatie de software zal gebruikt worden.*

## Planning

> *Geef een tijdsindicatie aan over de duur van het maken van dit document.
Beschrijf per onderdeel van het document hoeveel tijd je gaat gebruiken voor de
realisatie ervan.*

## Functionele eisen / Requirements

> *Beschrijf de gevraagde functionaliteiten. Vanuit het standpunt van de
verschillende actoren. Geef een opsomming van alle actoren die nodig zijn.*

## Onmogelijkheden

> *Beschrijf alle functionaliteiten waarbij de kans op  een onmogelijkheid tot
realiseren bestaat, en benoem de alternatieven.*

## Use case diagram / wireframes

> Insert them here

## Akkoord leidinggevende / Projectleider

Naam

---

Datum

---

Handtekening

---