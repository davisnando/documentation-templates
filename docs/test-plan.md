# Test Plan (__Projectnaam__)

```text
Leereenheid: NAAM LEEREENHEID
Versienummer: 1.0
Auteur(s): AUTEUR
Datom: DATUM
```

```text
COMPETENTIES

1. Competentie ‘Materialen en middelen inzetten’
    a. Kiest en maakt gebruik van materialen en middelen om de werking en
       functionaliteit van de gerealiseerde applicatie te testen
2. Competentie ‘Vakdeskundigheid toepassen’
    b. Gebruikt eerder opgedane kennis en ervaringen (mede uit de opleiding) om de
       testactiviteiten uit te voeren
3. Competentie ‘Creëren en innoveren’
    c. Beoordeelt tijdens het testen of er naar aanleiding van de testresultaten
       aanpassingen of veranderingen doorgevoerd moeten worden
4. Competentie ‘Analyseren’
    d. Verzamelt bij het testen gegevens/resultaten en komt tot onderbouwde conclusies
    e. Draagt waar nodig oplossingen aan en zet vervolgacties uit
5. Competentie ‘Formuleren en rapporteren’
    f. Documenteert alle testresultaten en indien nodig aangedragen oplossingen
       en vervolgacties
```

## Versiebeheer

| Datum | Versie | Wijziging | Wie |
| - | - | - | - |
| DATUM | 1.0 | Initial version | Bram |

## Inleiding

> *Beschrijf de doelstelling van dit document.*

## Testorganisatie

> *Benoem de personen die gaan testen, indien aanwezig de coordinator en/of testmanager.*

## Benodigde informatie voor testen

> *Inloggegevens: welke inloggegevens worden gebruik*
>
> *Databestanden/testdata: waar staan externe databestanden die in de test
> gebruikt worden*
>
> *Extra informatie: waar staan extra documenten ter verduidelijking van de applicatie*

## Test Cases

```text
Volgnummer:
Situatie:
Actie:
Verwacht Resultaat:
Correct Ja/Nee:
Opmerkingen:
Verrichte handelingen:
Aanpassen Ja/Nee:
Prioriteit:
Naam tester:
Datum getest:
Richtlijnen wijzigingen:
Datum opgelost:
Naam ontwikkelaar:
Oplossing:
```